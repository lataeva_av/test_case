﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium;
using Keys = OpenQA.Selenium.Keys;
using System.Collections;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Chrome;
using System.Runtime.Remoting.Contexts;

namespace WindowsFormsApp1
{
    
    public partial class Form1 : Form
    {
        
        IWebDriver Browser;
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
           
        Browser = new ChromeDriver();
        Browser.Manage().Window.Maximize();
        Browser.Navigate().GoToUrl("https://yandex.ru/");
            Browser.FindElement(By.XPath("//a[@class='home-link home-link_blue_yes home-tabs__link home-tabs__search'][5]")).SendKeys(Keys.Enter);
            Browser.FindElement(By.XPath("/html/body/div[1]/div/div[2]/noindex/ul/li[1]/a")).SendKeys(Keys.Enter);
            Browser.FindElement(By.LinkText("Мобильные телефоны")).Click();
            Browser.FindElement(By.XPath("//*[@id='search-prepack']/div/div/div[2]/div/div[1]/div[4]/fieldset/ul/li[10]/div/a/label")).Click();
            IWebElement element = Browser.FindElement(By.Id("glpricefrom"));
            element.SendKeys("40000");
            Thread.Sleep(5000);
            string el1 = Browser.FindElement(By.CssSelector("body > div > div:nth-child(6) > div.layout.layout_type_search.i-bem > div.layout__col.i-bem.layout__col_search-results_normal > div.n-filter-applied-results.metrika.b-zone.i-bem.n-filter-applied-results_js_inited.b-zone_js_inited > div > div.n-snippet-list.n-snippet-list_type_grid.snippet-list_size_3.metrika.b-zone.b-spy-init.i-bem.metrika_js_inited.snippet-list_js_inited.b-spy-init_js_inited.b-zone_js_inited > div:nth-child(1) > div.n-snippet-cell2__header > div.n-snippet-cell2__title > a")).GetAttribute("textContent").ToString();
            
            Browser.FindElement(By.XPath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).Click();
            string el3 = Browser.FindElement(By.CssSelector("body > div.main > div.n-product-summary.b-zone.i-bem.n-product-summary_js_inited.b-zone_js_inited > div.layout.layout_type_maya > div.n-product-summary__headline > div > div.n-product-title__text-container > div.n-product-title__text-container-top > div > h1")).GetAttribute("textContent").ToString();
            
            int i = String.Compare(el1, el3);
            if (i == 0)
            {
                label1.Text = "True";
            }
            else { label1.Text = "False"; }
            Thread.Sleep(5000);
            Browser.Quit();
         }

        private void button2_Click(object sender, EventArgs e)
        {
            Browser.Quit();          
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Browser = new ChromeDriver();
            Browser.Manage().Window.Maximize();
            Browser.Navigate().GoToUrl("https://yandex.ru/");
            Browser.FindElement(By.XPath("//a[@class='home-link home-link_blue_yes home-tabs__link home-tabs__search'][5]")).Click();
            Browser.FindElement(By.XPath("/html/body/div[1]/div/div[2]/noindex/ul/li[1]/a")).Click();
            Browser.FindElement(By.LinkText("Наушники и Bluetooth-гарнитуры")).Click();
            Browser.FindElement(By.LinkText("Beats")).Click();
            IWebElement el = Browser.FindElement(By.XPath("//*[@id='glpricefrom']"));
            el.SendKeys("17000");
            IWebElement el2 = Browser.FindElement(By.XPath("//*[@id='glpriceto']"));
            el2.SendKeys("25000");
            Thread.Sleep(5000);
            string el1 = Browser.FindElement(By.CssSelector("body > div.main > div:nth-child(6) > div.layout.layout_type_search.i-bem > div.layout__col.i-bem.layout__col_search-results_normal > div.n-filter-applied-results.metrika.b-zone.i-bem.n-filter-applied-results_js_inited.b-zone_js_inited > div > div.n-snippet-list.n-snippet-list_type_grid.snippet-list_size_3.metrika.b-zone.b-spy-init.i-bem.metrika_js_inited.snippet-list_js_inited.b-spy-init_js_inited.b-zone_js_inited > div:nth-child(1) > div.n-snippet-cell2__header > div.n-snippet-cell2__title > a")).GetAttribute("textContent").ToString(); 
            
            Browser.FindElement(By.XPath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).Click();
            string el3 = Browser.FindElement(By.CssSelector("body > div.main > div.n-product-summary.b-zone.i-bem.n-product-summary_js_inited.b-zone_js_inited > div.layout.layout_type_maya > div.n-product-summary__headline > div > div.n-product-title__text-container > div.n-product-title__text-container-top > div > h1")).GetAttribute("textContent").ToString();
            
            int i = String.Compare(el1, el3);
            if (i==0)
            {
                label3.Text = "True";
            }
            else { label3.Text = "False"; }

            Thread.Sleep(5000);
            Browser.Quit();
        }
        
        private void button4_Click(object sender, EventArgs e)
        {            
            string curTimeLong2 = DateTime.Now.ToString("dd/MM/yyyy HH-mm-ss");            
            Browser = new ChromeDriver();
            Browser.Manage().Window.Maximize();
             Browser.Navigate().GoToUrl("https://yandex.ru/");
            Thread.Sleep(5000);
            String el3 = Browser.FindElement(By.CssSelector("head > title")).GetAttribute("textContent").ToString(); 
            IWebElement el = Browser.FindElement(By.XPath("//*[@id='text']"));
            el.SendKeys("Альфа-Банк" + Keys.Enter);
            Browser.FindElement(By.LinkText("AlfaBank.ru")).Click();
            Thread.Sleep(5000);
            Browser.SwitchTo().Window(Browser.WindowHandles[1]);
            Thread.Sleep(5000);
            Browser.FindElement(By.LinkText("Вакансии")).Click();
            Browser.FindElement(By.XPath("/html/body/div[1]/div/nav/nav/span[5]/a/span")).Click();
            String el2 = Browser.FindElement(By.CssSelector("body > div.wrapper > div > div.main.clearfix > div.info")).GetAttribute("textContent").ToString();
            String s = Browser.GetType().Name.ToString();
            string a = curTimeLong2 + " " + el3 + " " + s + ".txt";
            using (FileStream fstream = new FileStream(a, FileMode.Create))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(el2);
                fstream.Write(array, 0, array.Length);
                fstream.Close();
            }
            
            Thread.Sleep(5000);
            Browser.Quit();
        }

       
        private void button5_Click(object sender, EventArgs e)
        {
            string line;
            List<int> list = new List<int>();
            StreamReader file = new StreamReader("numbers.txt");
            line = file.ReadLine();
            int[] a = line.Split(',').Select(int.Parse).ToArray();
            Array.Sort(a);
            foreach (int item in a)
            { 
                listBox1.Items.Add(item);
            }
            Array.Reverse(a);
            foreach (int item in a)
            {
                listBox2.Items.Add(item);
            }
        }

    }
}
